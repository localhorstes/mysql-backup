#!/bin/bash

WORKING_DIRECTORY=$(dirname "$(readlink -f "$0")")

if ! [ -f "${WORKING_DIRECTORY}/mysql-backup.conf" ]; then
  echo "mysql-backup.conf not found. Create from mysql-backup.conf.dist, please."
  exit -1
fi
. "${WORKING_DIRECTORY}/mysql-backup.conf"

DATABASES=($(/usr/bin/mysql --user=${DB_USER} --password=${DB_PASSWD} --execute="SHOW DATABASES")) # Alle vorhandenen DBs ermitteln und in ein Array überführen.

for ((i=1;i<${#DATABASES[@]};i+=1)); do
   if [ ${DATABASES[$i]} == "information_schema" ]; then MYSQL_OPTS="--skip-lock-tables"; fi
   if [ ${DATABASES[$i]} == "mysql" ]; then MYSQL_OPTS="--events"; fi
    if [ ${DATABASES[$i]} == "performance_schema" ]; then MYSQL_OPTS="--skip-lock-tables"; fi
   /usr/bin/mkfifo ${BACKUP_PATH}pipe_mysqldump
   /bin/gzip -9 -c < ${BACKUP_PATH}pipe_mysqldump > ${BACKUP_PATH}${DATABASES[${i}]}.gz &
   /usr/bin/mysqldump --opt --routines --triggers --user=${DB_USER} --password=${DB_PASSWD} --databases ${DATABASES[${i}]} ${MYSQL_OPTS} > ${BACKUP_PATH}pipe_mysqldump
   /bin/rm ${BACKUP_PATH}pipe_mysqldump
done

exit 0
